FROM nginx:1.15.8-alpine

MAINTAINER FearNixx Technik, <technik@fearnixx.de>

RUN apk update \
	&& apk upgrade \
	&& apk add --no-cache --update curl ca-certificates openssl git tar unzip bash \
	&& adduser -D -h /home/container container

USER container
ENV USER container
ENV HOME /home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/sh", "entrypoint.sh"]
